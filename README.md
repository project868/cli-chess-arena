#Chess-On-Command-Line

#Documentation

#A) Display Chess Board

	To create the Chess board, I have used default constructor of ChessBoard class. I have implemented all of the Square objects and Piece objects in default constructor of ChessBoard class. The board has 64 Square objects and some of them have Piece objects.

	To display the Chess board,I have overridden toString method of Object class. I created a String called brdStr, and I implemented the board to brdStr by using for loops. How to print all elements on the board was a challenge, for that I used nested for loops to reach each element of the board.
 
#B) Defining Chess Board and Square Classes

	I created a multi-dimensional Square Array using board2d reference from Square class in ChessBoard. I declared the board2d as public because I needed to reach it from other classes. 
	
	The board2d object is a multi-dimensional Square Array therefore, I created Square objects for each field of board2d in default constructor of ChessBoard class. Also I created pieces in default constructor of ChessBoard class.
 
#C) Implementing methods of Board and Square Classes

# Methods of Board (ChessBoard Class)

# public void decrementPiece(int color, Square location)

	> Decrements the number of pieces according to color. If color equals 0, decrements the number of white pieces, if not decrements the number of black pieces.
	> color: color of piece which will be decremented.
	> It doesn't return anything.
	> It checks color, if it is white, method decrements the number of white pieces, else it  decrements the number of black pieces.

# public String toString()
	
	> It holds brdStr variable which have board2d’s String form.
	> Variable:
		- It returns a String form of board.
		- It uses for loops to reach each element of the board.

# public boolean isGameEnded()

	> It checks if the game ended or not.
	> Variable:
		- It returns true if the game is ended, if not it returns false.
		- It checks the number of pieces, if one of them is zero it returns true. Also it controls there are 2 kings, if there is only 1 king it returns true again.

# public boolean isWhitePlaying()

	> It checks whose turn is it.
	> Variable: 
		- It returns true if white is playing, else it returns false.
		- It simply returns isWhite variable which controls with  nextPlayer().

# public Piece getPieceAt(String from)

	> It returns a Piece object according to its parameter .
	> “from” is a string which represents our selected piece’s location.
	> It returns a piece object of given co-ordinates.


# public int[] getCoordinates(String from)

	> It converts String to an Integer Array which have co-ordinates.
	> “from” is a String which represents our selected piece’s co-ordinates.
	> It returns an Integer Array.
	> I used charAt() method of String class to reach characters in the String. And then, I converted them Integer via using a Character:Integer Hasthtable.

# private void createHashtable()

	> It puts Characters and Integers which are on the board to Hashtable.
	> Variable:
	> Return: 
		- This method does not returns anything because the only task of this method is to put Characters and Integers which are on the board to Hashtable.

# public Square getSquareAt(String to)

	> Returns the Square object on the given co-ordinates.
	> “to” is a String which represents our target Square’s co-ordinates.
	> It returns a Square object.
	> It uses getCoordinates method to convert String “to” to the co-ordinates. And then, it returns Square according to these co-ordinates from the board2d.

# public Square[] getSquaresBetween(Square location, Square targetLocation)

	> Returns the squares between given squares if they are at the same row or same column or same diagonal.
	>location: the beginning square, targetLocation: the final square.
	> Array of squares as the same order from location to targetLocation square not including location. Null is returned if no squares are in between.
	> First, it controls given squares are at the same row or same column or same diagonal. If they are, it takes co-ordinates of given squares and it calculates other squares between location and targetLocation, then creates a square array and implements squares between location and targetLocation to this array. Finally it reverses the array and returns.

# public void nextPlayer()
	
	> Changes isWhite  value. If it is true, nextPlayer() makes it false. Else true.

# public Square[] getSquaresBetweenForKnight(Square location, Square targetLocation)

	> Returns the squares between given squares.
	> location: the beginning square, targetLocation: the final square
	> It returns an array of squares. The array only contains two squares targetLocation and other square which can prevent Knight's jump. (Knight can only jump over 1 unit)
	>Firstly, it creates a Square array which has size 2. And then adds squares to this array. It compares targetLocation’s coordinates and location’s coordinates, thanks to comparison it determines location of other square which can prevent moving of Knight.


# Methods of Square Class

# public String toString()

	> Returns “ “ if piece in square null else returns piece’s toString() method.

# public void setPiece(Piece piece)
	
	> If square has a piece, this means there is attacking. So that, I will decrement pieces number.

# public boolean isAtSameColumn(Square targetLocation)

	> It checks targetLocation and this square object is in the same column. If it is, it returns true else false.
	> targetLocation: location which we want to go.
	> It returns boolean value, returns true If targetLocation’s column equals square’s column else returns false.
	>It simply checks if the square’s column and  targetLocation’s column same.

# public boolean isAtSameDiagonal(Square targetLocation)

	> It checks targetLocation and this square object is in the same diagonal. If it is, it returns true else false.
	> targetLocation: location which we want to go.
	> It returns boolean value. Returns true If difference between targetLocation’s and square’s row is equal to difference between targetLocation’s and square’s column else returns false.
	> It simply checks if the square and targetLocation are at the same diagonal via using difference of rows and columns.

# public boolean isAtSameRow(Square targetLocation)

	> It works with similar to the isAtSameColumn. It checks targetLocation and square are in the same row. If they are, returns true, else returns false.
	> targetLocation: location which we want to go.
	> It returns boolean value, returns true if targetLocation’s row equals square’s row else returns false.
	>It simply checks if the square’s row and targetLocation’s row is same.

# public boolean isEmpty()

	> It checks square is empty.
	> Variable:
		- It returns boolean  value. When square is empty returns true, in the other case returns false.
		- It simply checks piece’s value. If it is null, this means square is empty. Else square is not empty.

# public boolean isNeighbourColumn(Square targetLocation)

	> It checks targetLocation is in the neighbour column of square.
	> targetLocation: location which we want to go.
	>  It returns boolean value. When targetLocation is in the neighbour column of square returns true, in the other case it returns false.
	> It checks difference of columns of square’s and targetLocation’s. If it is 1, this means their columns are neighbours.

# public boolean isAtLastRow(int color)

	> Checks whether the row is the last row. It uses color to learn is it last row for which color pieces.
	> color: It is an integer. Color is white for 0 and it is black for 1.
	> It returns true if the row is the last row, in the other case it returns false. It compares color and ChessBoard.WHITE static variables. 
	> It compares given color and ChessBoard.WHITE. 
	> If the given color is white, it checks that the square's row is equal to 0. And isAtLastRow() returns value of this.row == 0
	> If the given color is not equal to ChessBoard.WHITE, this means color is black. And 
color == ChessBoard.WHITE is false. If it is false, it checks that the square’s row is equal to 7. And isAtLastRow() returns the value of this.row == 7


# D) Defining Piece Hierarchy
  
  	The Piece class is an abstract class because actually there is no piece object in the board. On the board, there are Pawn, Bishop, King… objects. Pawn, Bishop, King and other objects also are a piece of chess game. Also, it is a superclass of Pawn, Bishop, King, Knight, Queen and Rook classes. 
	It is a parent class of these classes because all of these classes have similar methods, for example move() method is the same for almost all piece classes. Piece class has location and color. Since, Piece has location and color attributes subclasses of Piece inherit these attributes. 
  	
- public int getColor() method is not abstract because all of the pieces have color attributes. Instead of creating a getColor() method for each piece, creating one and using it is  easier because subclasses can reach getColor() method which is in the parent class.
  	
- public abstract boolean canMove(String to) is abstract because all of the pieces have different movement rules. We can’t control them with one canMove method.
 	
- public  void move(String to) is not abstract because all of the pieces can move with one move method  after checking they can move.
  
- protected boolean isEnemy(Piece p) is not abstract because it controls only color of piece and all pieces have color attributes so that they can use it. It is “protected” because it is used only by piece objects like Pawn, Knight… 
  
  	In the other hand, other methods are “public” because Main class to access these methods, they must be defined as “public”.

# E) Implementing methods in Piece Hierarchy

# public abstract boolean canMove(String to);

	> It checks piece can move to String to. Each  piece have this method so that it is abstract.
	> to: location which we want to go. It is string.
	> It returns boolean value for each piece in their classes.
	> It simply checks piece can move to given location or not.

# Methods of Pawn Class

# public boolean canMove(String to)

	> It checks targetLocation is at the same column with initial location, then it checks row distance between initial location and targetLocation. If row distance is 2 and color is white, this means white Pawn is moving twice. 
	> Squares between target and initial locations must be empty, it checks if they are empty or not. If they are empty it assigns true to validMove.
	> If targetLocation and initial location are not in the same column, Pawn can move only if is attacking diagonal.It checks again row distance and color, and assigns true target is not empty and targetLocation’s piece’s color is different from pawn.
	> Finally, if given if scopes doesn’t run, canMove returns false.

# public String toString()

	> It returns “P” if color is white (0), or it returns “p” if color is black (1).

# Methods of Rook Class

# public boolean canMove(String to)

	> It checks targetLocation and initial location are at the same column or same row. If they are not, it returns false. Because a rook cannot move diagonal, it can move only vertical or horizontal. 
	> Then, it checks if all of the squares between target are empty and it assigns boolean value of isEmpty() to validMove. Finally, it returns validMove.


# public String toString()

	>  It returns “R” if color is white (0), or it returns “r” if color is black (1).

# Methods of King Class:

# public boolean canMove(String to)

	> King can move everywhere but only 1 unit. So, it checks the first target location is in the 1 unit away and it is empty and returns the boolean value of this control. 
	> If validMove is false, and target location is in the 1 unit away, it checks there is an enemy in target location.  If there is an enemy, it makes validMove true and then it returns validMove.

# public String toString()

	>  It returns “K” if color is white (0), or it returns “k” if color is black (1).

# Methods of Queen Class:

# public boolean canMove(String to)

	> Queen can move everywhere if the target is in the diagonal, vertical or horizontal. So that, firstly it controls the target is in the same diagonal, same row or same column. 
	> If it is not, it returns false. If target is in the same diagonal, same row or same column it checks all squares between target and initial location and assigns boolean value of this control to validMove. 
	> After this control, if validMove becomes false, It checks there is an enemy at the target location. If there is, it makes validMove true. And finally, it returns validMove

# public String toString()

	> It returns “Q” if color is white (0), or it returns “q” if color is black (1).

# Methods of Bishop Class:

# public boolean canMove(String to)

	> Bishop can move only diagonal. Therefore, method checks if the target is at the diagonal. If it isn’t, it returns false. If target is at the same diagonal, it checks all squares between initial location and target are empty and then it assigns boolean value of this control to validMove.
	> If after then this control, validMove is false it checks if there is an enemy at the target location. 
	> If there is an enemy at the target location, it assigns true to validMove.And finally, method returns validMove.

# public String toString()

	> It returns “B” if color is white (0), or it returns “b” if color is black (1).

# Methods of Knight Class:

# public boolean canMove(String to)

	> Knight can move only by drawing a “L” shape. And the distance between target location and initial location must be 2.23607 units. 
	> So that method first calculates co-ordinates of target location and initial location. And then it measures distance between target and initial location. 
	> Secondly, it converts this value to DecimalFormat and then assigns it to a string.  It checks the distance between initial location and target location is equal to “2.24” (2.23607’s decimal format which is converted to string) If it is not equal, it returns false. 
	> If it is equal, it controls only the target and the other square (that the Knight can’t jump on it) are empty. If these squares are empty. It assigns a boolean value of isEmpty() to validMove for each control. 
	> If validMove becomes false after this checking process, it checks if there is an enemy at the target location. If it finds an enemy at the target location, assigns true to validMove. Finally it returns validMove.


# public String toString()

	>  It returns “N” if color is white (0), or it returns “n” if color is black (1).