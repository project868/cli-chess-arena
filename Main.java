
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import Chess.ChessBoard;
import Chess.Piece;

public class Main {
    public static void main(String[] args) {
    
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    
        // Create a chessboard instance
        ChessBoard board = new ChessBoard();
    
        //print the board
        System.out.println(board);

        //while game not ended request moves from the user
        while(!board.isGameEnded()){
    
            System.out.println("It is " + (board.isWhitePlaying() ? "White" : "Black") + "'s turn");
            Piece piece = null;

            //While the given coordinate does not contain the piece of the current player
            //request a coordinate
            String from =null;
       
            do {
       
                System.out.print("Enter the location of the piece:");
       
                try{
                    from = reader.readLine();
                }catch(IOException io){
                    io.printStackTrace();
                }
       
                //get the piece at the given location
                // null means there is the Square is empty
                piece = board.getPieceAt(from);
                System.out.println("Your Choice: "+piece);

                //piece color and current player's color should be consistent

            }while(piece == null || piece.getColor()!=(board.isWhitePlaying() ? ChessBoard.WHITE : ChessBoard.BLACK));

            String to = null;
            
            //while the target coordinate is not valid Square to move, request new coordinate
            do {
            
                System.out.print("Enter the new location of the piece:");
            
                try{
                    to = reader.readLine();
                }catch(IOException io){
                    io.printStackTrace();
                }
            }while(!piece.canMove(to));
            
             piece.move(to);
             System.out.println(board);
        }
    }
}
